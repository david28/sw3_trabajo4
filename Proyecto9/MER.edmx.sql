
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/17/2015 23:20:45
-- Generated from EDMX file: C:\Users\DAVID\Desktop\sw3\Proyecto9\Proyecto9\MER.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[usuarioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[usuarioSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'usuarioSet'
CREATE TABLE [dbo].[usuarioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'personaSet'
CREATE TABLE [dbo].[personaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre_persona] nvarchar(max)  NOT NULL,
    [correo_persona] nvarchar(max)  NOT NULL,
    [telefono_persona] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'usuarioSet'
ALTER TABLE [dbo].[usuarioSet]
ADD CONSTRAINT [PK_usuarioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'personaSet'
ALTER TABLE [dbo].[personaSet]
ADD CONSTRAINT [PK_personaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------