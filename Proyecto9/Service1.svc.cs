﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Proyecto9
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        private MERContainer db = new MERContainer();

        public List<usuario> mostrasusuarios()
        {
            return db.usuarioSet.ToList();

        }
        
        public usuario mostras1usuarios(string Id)
        {
            int nId = Convert.ToInt16(Id);
            return db.usuarioSet.Where(p => p.Id == nId).First();
        }

        
        public List<persona> mostrarpersonas()
        {
            return db.personaSet.ToList();

        }
    }
}
