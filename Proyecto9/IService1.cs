﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Proyecto9
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrasusuarios", ResponseFormat = WebMessageFormat.Json)]
        List<usuario> mostrasusuarios();


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarusuario/{Id}", ResponseFormat = WebMessageFormat.Json)]
        usuario mostras1usuarios(string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "mostrarpersonas", ResponseFormat = WebMessageFormat.Json)]
        persona mostrarpersonas();
       

        // TODO: agregue aquí sus operaciones de servicio
    }


    
}
